# eeg_neuromarkers

## Functionally annotated electrophysiological neuromarkers of healthy ageing and memory function

This project contains data and scripts associated with the paper:

Functionally annotated electrophysiological neuromarkers of healthy ageing and memory function
Tibor Auer, Robin Goldthorpe, Robert Peach, Henry Hebron, Ines R. Violante

